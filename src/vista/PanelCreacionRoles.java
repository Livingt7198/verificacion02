package vista;

import javax.swing.JOptionPane;
import modelo.CrearRolesModelo;
import modelo.Rol;

public class PanelCreacionRoles extends javax.swing.JPanel {

    private PrincipalJFrame principalJFrame;
    private CrearRolesModelo rolesModelo;
    private Rol roles;
    private AreaDeTrabajoJPanel areaDeTrabajoJPanel;

    public PanelCreacionRoles(PrincipalJFrame principalJFrame) {
//        this.areaDeTrabajoJPanel = areaDeTrabajoJPanel;
        this.principalJFrame = principalJFrame;
//        this.tablaCrearRoles.setVisible(false);
//        this.actualizarTablaRoles()
        initComponents();

    }

    public void PanelCreacionRoles() {
        actualizarTablaRoles();
    }

    public void reiniciarComponentesCrearRoles() {
        txtIdRol.setText("");
        txtNombreRol.setText("");

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtIdRol = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtNombreRol = new javax.swing.JTextField();
        btnCrearRol = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaCrearRoles = new javax.swing.JTable();
        btnEliminarRol = new javax.swing.JButton();

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Crear roles"));

        jLabel1.setText("Nombre rol");

        txtIdRol.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtIdRolActionPerformed(evt);
            }
        });

        jLabel2.setText("Descripción rol");

        btnCrearRol.setText("Crear Rol");
        btnCrearRol.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCrearRolActionPerformed(evt);
            }
        });

        tablaCrearRoles.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "identificador Rol", "Nombre Rol"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tablaCrearRoles.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaCrearRolesMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tablaCrearRoles);

        btnEliminarRol.setText("Eliminar Rol");
        btnEliminarRol.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarRolActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 47, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtNombreRol, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtIdRol, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(43, 43, 43)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnEliminarRol, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnCrearRol, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(38, 38, 38))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtIdRol, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCrearRol))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnEliminarRol)
                    .addComponent(txtNombreRol, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(43, 43, 43)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(28, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void txtIdRolActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtIdRolActionPerformed

    }//GEN-LAST:event_txtIdRolActionPerformed

    private void btnCrearRolActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCrearRolActionPerformed
        crearRol();
    }//GEN-LAST:event_btnCrearRolActionPerformed

    private void btnEliminarRolActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarRolActionPerformed
        eliminarRol();
    }//GEN-LAST:event_btnEliminarRolActionPerformed

    private void tablaCrearRolesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaCrearRolesMouseClicked
        btnEliminarRol.setEnabled(true);
    }//GEN-LAST:event_tablaCrearRolesMouseClicked

    public void actualizarTablaRoles() {
        rolesModelo = new CrearRolesModelo(principalJFrame.getGestion().getRoles());
        tablaCrearRoles.setModel(rolesModelo);
    }

    public void crearRol() {
        if (txtIdRol.getText().equals("") || txtNombreRol.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "Id rol y Nombre rol son obligatorios");
        } else {

            principalJFrame.getGestion().crearRol(txtIdRol.getText(), txtNombreRol.getText());

            tablaCrearRoles.setEnabled(true);
            actualizarTablaRoles();
            reiniciarComponentesCrearRoles();
        }
    }

    public void eliminarRol() {
        if (tablaCrearRoles.getSelectedRow() < 0) {
            JOptionPane.showMessageDialog(this, "Debe seleccionar un rol para eliminarlo");
        } else {
            roles = (Rol) rolesModelo.getValueAt(tablaCrearRoles.getSelectedRow());
            principalJFrame.getGestion().eliminarRol(roles.getName());
            actualizarTablaRoles();
        }

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCrearRol;
    private javax.swing.JButton btnEliminarRol;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tablaCrearRoles;
    private javax.swing.JTextField txtIdRol;
    private javax.swing.JTextField txtNombreRol;
    // End of variables declaration//GEN-END:variables

}
