package controlador.logica;

import java.util.ArrayList;
import modelo.Rol;
import modelo.Usuario;

public class Gestion {

    private ArrayList<Usuario> usuarios;
    private ArrayList<Rol> roles;
    private Usuario usuarioEnSesion;
    private Usuario superUsuario;
    private Rol superRol;

    public Gestion() {
        roles = new ArrayList<Rol>();
        usuarios = new ArrayList<Usuario>();
        superUsuario = new Usuario("argus", "1234", "Argusito", false);
        superRol = new Rol("Administrador", "El Admin");
        superUsuario.setRol(superRol);
        usuarios.add(superUsuario);
        roles.add(superRol);
        usuarioEnSesion = null;
        invarianteGestion();
    }

    private void invarianteGestion() {
        assert roles != null : "no existen roles";
        assert usuarios != null : "no existen usuarios";
        assert assertRoles("Administrador") : "El rol administrador no existe";
        assert assertGetUser("argus") : "El usuario administrador no existe";
    }

    /**
     *
     * @param user
     * @param pass
     * @return
     */
    public boolean iniciarSesion(String user, String pass) {
        try {
            //pre
            assert assertUser(user, pass) : "Fallo la precodicion";

            //logica
            int tam = usuarios.size();
            usuarioEnSesion = obtenerUsuario(user);
            usuarios.remove(usuarioEnSesion);
            usuarioEnSesion.setEstado(true);
            usuarios.add(usuarioEnSesion);

            //pos
            assert assertUser(user) : "Fallo la postcondicion";
            assert usuarios.size() == tam : "Usuario agregado";
            return true;
        } catch (AssertionError e) {
            System.out.println("falla por: " + e.toString());
            return false;
        }
    }

    public boolean cerrarSesion() {
        try {
            // Pre
            String user = usuarioEnSesion.getUser();
            assert assertInSession(user) : "Fallo precon cerrar sesion";
            //Logica
            usuarios.remove(usuarioEnSesion);
            usuarioEnSesion.setEstado(false);
            usuarios.add(usuarioEnSesion);
            usuarioEnSesion = null;

            //Post
            assert usuarioEnSesion == null : "Fallo post cerrar sesion";
            assert assertOutSession(user) : "Fallo post cerrar sesion";
            return true;
        } catch (AssertionError e) {
            System.out.println("falla por: " + e.toString());
            return false;
        }
    }

    public boolean eliminarUsuario() {

        try {
            //Pre
            String user = usuarioEnSesion.getUser();
            assert assertUser(user) : "Error user no registrado Delete User";
            assert assertInSession(user) : "Error user no esta  en sesion Delete User";
            //Logica
            usuarios.remove(usuarioEnSesion);
            usuarioEnSesion.setEstado(false);

            //Post
            assert !assertInSession(user) : "Falla estado Delete User";
            assert !assertGetUser(user) : "Falla el usuario sigue en el sistema Delete user";
            return true;

        } catch (AssertionError e) {
            System.out.println("falla por: " + e.toString());
            return false;
        }
    }

    public boolean eliminarUsuario(String userInSession, String userEliminar) {
        try {
            //pre
            assert assertGetRoles() : "El usuario en sesion no es administrador  - método eliminar usuario 2";
            assert assertSuperUsuario(userEliminar) : "No se puede eliminar este usuario - es super Usuario";
            assert assertGetUser(userEliminar) : "Error user no registrado  - método eliminar usuario 2";

            //Logica
            Usuario usuarioAEliminar = obtenerUsuario(userEliminar);
            usuarios.remove(usuarioAEliminar);

            //post
            assert !assertGetUser(userEliminar) : "Falla el usuario sigue en el sistema - método eliminar usuario 2";
            return true;
        } catch (AssertionError e) {
            System.out.println("falla por: " + e.toString());
            return false;
        }
    }

    public boolean eliminarRol(String nameRol) {
        try {
            //pre
            assert assertRoles(nameRol) : "El rol no se encuentra Registrado";
            assert assertSuperRol(nameRol) : "No se puede eliminar este rol - es super Rol";
            assert assertUserRol(nameRol) : "No se puede eliminar este rol - tiene usuarios asignados";
//            assert !assertUserRol(nameRol) : "Existen usuarios con ese Rol" ;

            //logica
            Rol r = obtenerRol(nameRol);
            roles.remove(r);

            //post
            assert !assertRoles(nameRol) : "El rol aun existe en el sistema";

            return true;
        } catch (AssertionError e) {
            System.out.println("falla por: " + e.toString());
            return false;
        }
    }

    public boolean crearUsuario(String userName, String pass, String Nombre, String nameRol) {
        try {
            //pre
            assert !assertGetUser(userName) : "El usuario ya esta en el sistema";
            assert assertValidarUserName(userName) : "El nombre de usuario está mal escrito @ucentral.edu.co";
            assert pass.length() > 8 : "La contraseña debe ser mañor a 8 caracteres";
            assert assertRoles(nameRol) : "El rol seleccionado no se encuentra registrado";
            //Logica
            int tam = usuarios.size();
            Rol r = obtenerRol(nameRol);
            Usuario newUser = new Usuario(userName, pass, Nombre, false);
            newUser.setRol(r);
            usuarios.add(newUser);

            //post
            assert usuarios.size() != tam : "Usuario agregado";
            assert assertInSession(userName) : "Falla el estado del user CrearUsuario";

            return true;
        } catch (AssertionError e) {
            System.out.println("falla por: " + e.toString());
            return false;
        }
    }

    public boolean crearRol(String nameRol, String descripcion) {
        try {
            //pre
            assert !assertRoles(nameRol) : "Falló pre condición - El rol ya existe en el sistema";

            //Logica
            int tam = roles.size();
            Rol newRol = new Rol(nameRol, descripcion);
            roles.add(newRol);

            //post
            assert roles.size() != tam : "Falló post - Rol agregado - crear rol";
            return true;

        } catch (AssertionError e) {
            System.out.println("Falla por: " + e.toString());
            return false;
        }
    }

    public void salir() {
        //el usuario debe salir//cambiar estdo a false
        System.exit(0);
    }

    /**
     * ***************ASSERTS*************
     */
    /**
     *
     * @param user
     * @return retorna un usuario
     */
    private Usuario obtenerUsuario(String user) {
        for (int i = 0; i < usuarios.size(); i++) {
            if (usuarios.get(i).getUser().equals(user)) {
                return usuarios.get(i);
            }
        }
        return null;
    }

    /**
     *
     * @param nameRol
     * @return retorna un rol
     */
    private Rol obtenerRol(String nameRol) {
        for (int i = 0; i < roles.size(); i++) {
            if (roles.get(i).getName().equals(nameRol)) {
                return roles.get(i);
            }
        }
        return null;
    }

    /**
     *
     * @param user
     * @param pass
     * @return
     */
    private boolean assertUser(String user, String pass) {
        for (int i = 0; i < usuarios.size(); i++) {
            if (usuarios.get(i).getUser().equals(user) && usuarios.get(i).getPassword().equals(pass)) {
                if (!usuarios.get(i).isEstado()) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        return false;
    }

    /**
     *
     * @param user
     * @return
     */
    private boolean assertUser(String user) {
        for (int i = 0; i < usuarios.size(); i++) {
            if (usuarios.get(i).isEstado() && usuarios.get(i).getUser().equals(user)) {
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @param user
     * @return devuelve si el usuario existe en el sistema
     */
    private boolean assertGetUser(String user) {
        for (int i = 0; i < usuarios.size(); i++) {
            if (usuarios.get(i).getUser().equals(user)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * 
     * @param nombreUsuario
     * @return devuelve si el usuario es super Usuario
     */
    private boolean assertSuperUsuario(String nombreUsuario) {
        if (nombreUsuario.equalsIgnoreCase(superUsuario.getUser())) {
            return false;
        }

        return true;
    }

    /**
     *
     * @param user
     * @return
     */
    private boolean assertInSession(String user) {
        for (int i = 0; i < usuarios.size(); i++) {
            if (usuarios.get(i).isEstado()) {
                return true;
            }
        }
        return false;
    }

    /**
     * 
     * @param nombreUser
     * @return retorna el valor en el que se encuentra el estado del usuario
     */
    private boolean assertEstadoUsuario(String nombreUser) {
        Usuario usuario = obtenerUsuario(nombreUser);
        System.out.println("estado usuario: " + usuario.isEstado());
        if (usuario.isEstado()) {
            return true;
        }
        return false;
    }
    
    /**
     * 
     * @param nombreUser
     * @return valida que el nombre de usuario contenga @ucentral.edu.co
     */
    private boolean assertValidarUserName(String nombreUser) {
        String[] parts = nombreUser.split("@");
        if(parts.length == 1){
            return false;
        }
        if(parts[1].equalsIgnoreCase("ucentral.edu.co")){
           return true; 
        }
        return false;
    }

    /**
     *
     * @param user
     * @return
     */
    private boolean assertOutSession(String user) {
        for (int i = 0; i < usuarios.size(); i++) {
            if (!usuarios.get(i).isEstado()) {
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @param rol
     * @return devuelve si el rol ya existe en el sistema
     */
    private boolean assertRoles(String rol) {
        for (int i = 0; i < roles.size(); i++) {
            if (roles.get(i).getName().equalsIgnoreCase(rol)) {
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @param nombreRol
     * @return devuelve si el rol es superRol
     */
    private boolean assertSuperRol(String nombreRol) {
        if (nombreRol.equalsIgnoreCase(superRol.getName())) {
            return false;
        }

        return true;
    }

    /**
     *
     * @return devuelve si el usuario en sesion es Administrador
     */
    public boolean assertGetRoles() {

        if (usuarioEnSesion.getRol().getName().equalsIgnoreCase("Administrador")) {
            return true;
        }
        return false;
    }

    /**
     *
     * @param nameRol
     * @return devuelve si existen usuarios asignados a ese rol
     */
    private boolean assertUserRol(String nameRol) {
        for (int i = 0; i < usuarios.size(); i++) {
            if (usuarios.get(i).getRol().getName().equalsIgnoreCase(nameRol)) {
                return false;
            }
        }
        return true;
    }

    public ArrayList<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(ArrayList<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    public ArrayList<Rol> getRoles() {
        return roles;
    }

    public void setRoles(ArrayList<Rol> roles) {
        this.roles = roles;
    }

    public Usuario getUsuarioEnSesion() {
        return usuarioEnSesion;
    }

    public void setUsuarioEnSesion(Usuario usuarioEnSesion) {
        this.usuarioEnSesion = usuarioEnSesion;
    }

}
