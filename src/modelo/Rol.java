
package modelo;


public class Rol {
    private String name;
    private String descripcion;

    public Rol(String name, String descripcion) {
        this.name = name;
        this.descripcion = descripcion;
    }
     private void invarianteRol() {
        assert name != "" : "no tiene name";
        assert descripcion != "" : "no tiene asignado una descripsión";        
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return   name ;
    }
    
    
    
    
}
