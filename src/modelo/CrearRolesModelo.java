package modelo;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

public class CrearRolesModelo extends AbstractTableModel {

    private ArrayList<String> titulos;
    private List<Rol> arrayCrearRoles;

    public CrearRolesModelo() {
    }
    public CrearRolesModelo(List<Rol> arrayCrearRol) {
        this.titulos = new ArrayList();
        titulos.add("Nombre rol");
        titulos.add("Descripción rol");
        this.arrayCrearRoles = arrayCrearRol;
    }

    public ArrayList getTitulos() {
        return titulos;
    }

    public void setTitulos(ArrayList titulos) {
        this.titulos = titulos;
    }

    @Override
    public int getRowCount() {
        return arrayCrearRoles.size();
    }

    @Override
    public int getColumnCount() {
        return titulos.size();
    }

    @Override
    public Object getValueAt(int fila, int col) {
        Rol crearRoles = arrayCrearRoles.get(fila);

        switch (col) {
            case 0:
                return crearRoles.getName();
            case 1:
                return crearRoles.getDescripcion();
            default:
                return null;
        }
    }

    public Object getValueAt(int rowIndx) {
        return arrayCrearRoles.get(rowIndx);
    }

    @Override
    public String getColumnName(int column) {
        return titulos.get(column);
    }

    public void removeRow(int row) {
       fireTableRowsDeleted(row,row);
    }
}
