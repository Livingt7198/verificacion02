
package modelo;

import modelo.Usuario;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

public class CrearUsuariosModelo extends AbstractTableModel {

    private ArrayList<String> titulos;
    private ArrayList<Usuario> arrayUsuarios;

    public CrearUsuariosModelo(ArrayList<Usuario> arrayUsuarios) {
        this.titulos = new ArrayList();
        titulos.add("Nick usuario");
        titulos.add("Nombre usuario");
        titulos.add("Tipo de rol");
        this.arrayUsuarios = arrayUsuarios;
    }
    
    
    @Override
    public int getRowCount() {
        return arrayUsuarios.size();
    }

    @Override
    public int getColumnCount() {
        return titulos.size();
    }

   @Override
    public Object getValueAt(int fila, int col) {
        Usuario usuario = arrayUsuarios.get(fila);

        switch (col) {
            case 0:
                return usuario.getUser();
            case 1:
                return usuario.getNombre();
            case 2:
                return usuario.getRol().getName();
            default:
                return null;
        }
    }

    public Object getValueAt(int rowIndx) {
        return arrayUsuarios.get(rowIndx);
    }

    @Override
    public String getColumnName(int column) {
        return titulos.get(column);
    }
    
}
