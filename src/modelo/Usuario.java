/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author estudiante
 */
public class Usuario {

    private String user;
    private String password;
    private String nombre;
    private boolean estado;
    private Rol rol;

    public Usuario() {
    }

    public Usuario(String user, String password, String nombre, boolean estado) {
        this.user = user;
        this.password = password;
        this.nombre = nombre;
        this.estado = estado;
        invarianteUser();
    }

     private void invarianteUser() {
        assert user != "" : "no tiene user";
        assert password != "" : "no tiene asignado password";
        assert nombre != "" : "no tiene nombre ";
        assert estado != true: "El usuario administrador no existe";
    }

    
    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public Rol getRol() {
        return rol;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }
    
    
    @Override
    public String toString() {
        return "Usuario{" + "user=" + user + ", password=" + password + ", nombre=" + nombre + ", estado=" + estado + ", rol=" + rol + '}';
    }

   

}
